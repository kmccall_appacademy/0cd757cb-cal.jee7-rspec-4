class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entries)
    if entries.is_a?(String)
      @entries[entries] = nil
    elsif entries.is_a?(Hash)
      @entries.merge!(entries)
    end
  end

  def find(partial)
    @entries.select do |key, value|
      key.include?(partial)
    end
  end

  def include?(key)
    @entries.has_key?(key)
  end

  def keywords
    @entries.keys.sort { |x, y| x <=> y }
  end

  def printable
    entries = keywords.map do |keyword|
      "[" + keyword + "] \"" + @entries[keyword] + "\""
    end
    entries.join("\n")
  end

end
