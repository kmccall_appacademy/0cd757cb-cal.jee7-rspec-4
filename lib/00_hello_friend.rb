class Friend
  def greeting(someone=nil)
    if !someone.nil?
      "Hello, #{someone}!"
    else
      "Hello!"
    end
  end
end
