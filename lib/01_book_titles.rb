class Book
  attr_accessor :title

  def title=(title)
    dont_capitalize = ["a", "an", "and", "the", "in", "of"]
    words = title.split
    capitalized_words = [words[0].capitalize]
    words[1..-1].each do |word|
      if !dont_capitalize.include?(word)
        capitalized_words << word.capitalize
      else
        capitalized_words << word
      end
    end
    @title = capitalized_words.join(" ")
  end

end
