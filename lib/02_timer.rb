class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    time_seconds = seconds % 60
    time_minutes = (seconds / 60) % 60
    time_hours = (seconds / 60 / 60) % 24

    "#{pad(time_hours)}:#{pad(time_minutes)}:#{pad(time_seconds)}"
  end

  def pad(time)
    time_str = time.to_s
    if time_str.length == 2
      return time_str
    else
      "0" + time_str
    end
  end
end
