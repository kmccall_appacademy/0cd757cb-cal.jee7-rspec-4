class Temperature
  def self.from_celsius(degrees)
    self.new(c: degrees)
  end

  def self.from_fahrenheit(degrees)
    self.new(f: degrees)
  end

  def self.ctof(degrees)
    degrees * 9.0 / 5 + 32
  end

  def self.ftoc(degrees)
    (degrees - 32) * 5.0 / 9
  end

  def initialize(options = {})
    if options[:f]
      self.fahrenheit = options[:f]
    else
      self.celsius = options[:c]
    end
  end

  def celsius=(degrees)
    @temperature = degrees
  end

  def fahrenheit=(degrees)
    @temperature = self.class.ftoc(degrees)
  end

  def in_celsius
    @temperature
  end

  def in_fahrenheit
    self.class.ctof(@temperature)
  end

end


class Celsius < Temperature
  def initialize(degrees)
    self.celsius = degrees
  end

end

class Fahrenheit < Temperature
  def initialize(degrees)
    self.fahrenheit = degrees
  end

end
